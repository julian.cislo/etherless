import { HardhatUserConfig } from "hardhat/config";

import "@nomicfoundation/hardhat-toolbox";
import "@nomicfoundation/hardhat-ethers";
import "hardhat-ignore-warnings";
import "hardhat-exposed";
import "solidity-docgen";
import "solidity-coverage";

import "@nomiclabs/hardhat-solhint";

const config: HardhatUserConfig = {
  solidity: "0.8.23",
};

module.exports = {  
  solidity: {
    version: "0.8.23",
    settings: {
      optimizer: {
        enabled: false,
        runs: 200,
      },
      viaIR: false,
      outputSelection: { '*': { '*': ['storageLayout'] } },
    },
  },
};



export default config;
