# Mobby Token

The Mobby token is an etherless, lockable token based on the OpenZeppelin Library.

To install and test this contract locally, you need nodejs installed. Then you can
run the following commands:

```shell
npm install
npx hardhat compile # compile solidity contracts
npx hardhat test # run unit tests
REPORT_GAS=true npx hardhat test # run unit tests with gas coverage
npx hardhat coverage # run code coverage tool
npx hardhat check # run linter
```
