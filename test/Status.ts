import {
    loadFixture,
} from "@nomicfoundation/hardhat-toolbox-viem/network-helpers";
import { expect } from "chai";
import hre from "hardhat";
import { getAddress, hexToNumber, parseGwei } from "viem";

describe("StatusCodeTester", function () {
    // We define a fixture to reuse the same setup in every test.
    // We use loadFixture to run this setup once, snapshot that state,
    // and reset Hardhat Network to that snapshot in every test.
    async function deployStatusFixture() {
        // Contracts are deployed using the first signer/account by default
        const [owner, otherAccount] = await hre.ethers.getSigners();

        var defaultCode = hexToNumber("0xFF");
        const status = await hre.ethers.deployContract("StatusCodeTester", [defaultCode], {
            value: defaultCode,
        });

        return {
            status,
            defaultCode,
            owner,
            otherAccount,
        };
    }

    // NOTE: These are here to make sure bad inserts aren't made
    describe("Smoke", function () {
        it("Sets correct defaultStatusCode", async function () {
            const { status, defaultCode } = await loadFixture(deployStatusFixture);
            expect(await status.defaultStatusCode()).to.equal(defaultCode);
        });
        it("Prints correct code for 0x0F", async function () {
            var expected = "INFORMATIONAL_OR_METADATA";
            var code = hexToNumber("0x0F");
            const { status } = await loadFixture(deployStatusFixture);
            expect(await status.getCode(code)).to.equal(expected)
        });
        it("Prints correct code for 0x1F", async function () {
            var expected = "PERMISSION_DETAILS_OR_CONTROL_CONDITIONS";
            var code = hexToNumber("0x1F");
            const { status } = await loadFixture(deployStatusFixture);
            expect(await status.getCode(code)).to.equal(expected)
        });
        it("Prints correct code for 0x2F", async function () {
            var expected = "MATCHING_META_OR_INFO";
            var code = hexToNumber("0x2F");
            const { status } = await loadFixture(deployStatusFixture);
            expect(await status.getCode(code)).to.equal(expected)
        });
        it("Prints correct code for 0x3F", async function () {
            var expected = "NEGOTIATION_RULES_OR_PARTICIPATION_INFO";
            var code = hexToNumber("0x3F");
            const { status } = await loadFixture(deployStatusFixture);
            expect(await status.getCode(code)).to.equal(expected)
        });
        it("Prints correct code for 0x4F", async function () {
            var expected = "AVAILABILITY_RULES_OR_INFO";
            var code = hexToNumber("0x4F");
            const { status } = await loadFixture(deployStatusFixture);
            expect(await status.getCode(code)).to.equal(expected)
        });
        it("Prints correct code for 0x5F", async function () {
            var expected = "TOKEN_OR_FINANCIAL_INFORMATION";
            var code = hexToNumber("0x5F");
            const { status } = await loadFixture(deployStatusFixture);
            expect(await status.getCode(code)).to.equal(expected)
        });
        it("Prints correct code for 0x9F", async function () {
            var expected = "RESERVED_0x9F";
            var code = hexToNumber("0x9F");
            const { status } = await loadFixture(deployStatusFixture);
            expect(await status.getCode(code)).to.equal(expected)
        });
        it("Prints correct code for 0xAF", async function () {
            var expected = "APP_SPECIFIC_META_OR_INFO";
            var code = hexToNumber("0xAF");
            const { status } = await loadFixture(deployStatusFixture);
            expect(await status.getCode(code)).to.equal(expected)
        });
        it("Prints correct code for 0xDF", async function () {
            var expected = "RESERVED_0xDF";
            var code = hexToNumber("0xDF");
            const { status } = await loadFixture(deployStatusFixture);
            expect(await status.getCode(code)).to.equal(expected)
        });
        it("Prints correct code for 0xEF", async function () {
            var expected = "CRYPTOGRAPHY_ID_OR_PROOF_METADATA"; // 0xEF
            var code = hexToNumber("0xEF");
            const { status } = await loadFixture(deployStatusFixture);
            expect(await status.getCode(code)).to.equal(expected)
        });
        it("Prints correct code for 0xFF", async function () {
            var expected = "OFF_CHAIN_INFO_OR_META"
            var code = hexToNumber("0xFF");
            const { status } = await loadFixture(deployStatusFixture);
            expect(await status.getCode(code)).to.equal(expected)
        });
    });
});