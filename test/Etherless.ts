import {
    loadFixture,
} from "@nomicfoundation/hardhat-toolbox-viem/network-helpers";
import { expect } from "chai";
import hre from "hardhat";
import {Signature, verifyTypedData} from "ethers";
import { hexlify, toUtf8Bytes } from "ethers";

export function runEtherlessTests(contractName: string) {

    async function deployEtherlessFixture() {
        const etherless = await hre.ethers.deployContract(contractName, [], {});
        const [owner, otherAccount] = await hre.ethers.getSigners();
    
        const domain = {
            name: await etherless.name(),
            version: "1",
            chainId: (await hre.ethers.provider.getNetwork()).chainId,
            verifyingContract: await etherless.getAddress(),
        };
    
        const types = {
            transferEtherless: [
                { name: "spender", type: "address" },
                { name: "recipient", type: "address" },
                { name: "value", type: "uint256" },
                { name: "fee", type: "uint256" },
                { name: "nonce", type: "uint256" },
                { name: "deadline", type: "uint256" },
            ],
            };

        if (contractName == "Mobby") {
            await etherless.mint(owner.address, 1000000n, hexlify(toUtf8Bytes("Initial Mint")));
            await etherless.unlock(owner.address, 1000000n);
        } else {
            await etherless.mint(owner.address, 1000000);
        }
    
        return {
            etherless,
            owner,
            otherAccount,
            domain,
            types
        };
    }

    describe("Etherless", function () {
        describe("Deployment", function () {
            it("Has 1 million owner balance", async function () {
                const { etherless, owner } = await loadFixture(deployEtherlessFixture);
    
                expect(await etherless.balanceOf(owner.address)).to.equal(1000000n);
            });
    
            it("Has empty other account balance", async function () {
                const { etherless, otherAccount } = await loadFixture(deployEtherlessFixture);
    
                expect(await etherless.balanceOf(otherAccount.address)).to.equal(0n);
            });
    
            it("Has 1million total supply", async function () {
                const { etherless } = await loadFixture(deployEtherlessFixture);
    
                expect(await etherless.totalSupply()).to.equal(1000000n);
            });
        });
        describe("Etherless Transactions", function() {
            it("Lets third party send to other account", async function() {
                const { etherless, domain, types } = await loadFixture(deployEtherlessFixture);
    
                const [owner, otherAccount, thirdParty] = await hre.ethers.getSigners();
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;
                const thirdAddr = thirdParty.address;
    
                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
    
    
                const thirdEtherless = new hre.ethers.Contract(
                    await etherless.getAddress(),
                    etherless.interface,
                    thirdParty,
                );
    
                const value = 500n;
                const fee = 20n;
    
                const nonce = await thirdEtherless.nonces(ownerAddr);
                const deadline = Math.ceil(Date.now() / 1000) + 5000;
                const values = {
                    spender: ownerAddr,
                    recipient: otherAddr,
                    value: value,
                    fee: fee,
                    nonce: nonce,
                    deadline: deadline,
                };
    
                const signatureHex = await owner.signTypedData(
                    domain,
                    types,
                    values,
                );
    
                const sigparts = Signature.from(signatureHex);
    
                // NOTE: this fails with 
                // "TypeError: Cannot read properties of undefined (reading 'spender')"
                // Appears to be a bug in the library...
                // const valid = await verifyTypedData({
                //     address: ownerAddr,
                //     domain,
                //     types,
                //     primaryType: 'transferEtherless',
                //     values,
                //     signatureHex,
                //   })
    
                await thirdEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigparts.v,
                    sigparts.r,
                    sigparts.s,
                );
    
                expect(await etherless.balanceOf(otherAddr)).to.equal(value);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(fee);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n - value - fee);
    
            });
            it("Stops third party send to other account when invalid signature", async function() {
                // We're going to do the obvious and sign as ourselves but try to transfer from the owner.
                const { etherless, domain, types } = await loadFixture(deployEtherlessFixture);
    
                const [owner, otherAccount, thirdParty] = await hre.ethers.getSigners();
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;
                const thirdAddr = thirdParty.address;
    
                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
    
    
                const thirdEtherless = new hre.ethers.Contract(
                    await etherless.getAddress(),
                    etherless.interface,
                    thirdParty,
                );
    
                const value = 500n;
                const fee = 20n;
    
                const nonce = await thirdEtherless.nonces(ownerAddr);
                const deadline = Math.ceil(Date.now() / 1000) + 5000;
                const values = {
                    spender: ownerAddr,
                    recipient: otherAddr,
                    value: value,
                    fee: fee,
                    nonce: nonce,
                    deadline: deadline,
                };
    
                // third party signs here, not owner. 
                const signatureHex = await thirdParty.signTypedData(
                    domain,
                    types,
                    values,
                );
    
                const sigparts = Signature.from(signatureHex);
    
                // NOTE: this fails with 
                // "TypeError: Cannot read properties of undefined (reading 'spender')"
                // Appears to be a bug in the library...
                const valid = await verifyTypedData(
                    domain,
                    types,
                    values,
                    signatureHex,
                  );
    
                await expect(thirdEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigparts.v,
                    sigparts.r,
                    sigparts.s,
                )).to.be.rejectedWith('EtherlessInvalidSigner');
    
                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
    
                // Make sure receiver can't do it either
                const signatureHexOther = await otherAccount.signTypedData(
                    domain,
                    types,
                    values,
                );
    
                const sigpartsOther = Signature.from(signatureHexOther);
    
                const otherEtherless = new hre.ethers.Contract(
                    await etherless.getAddress(),
                    etherless.interface,
                    otherAccount,
                );
    
                await expect(otherEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigpartsOther.v,
                    sigpartsOther.r,
                    sigpartsOther.s,
                )).to.be.rejectedWith('EtherlessInvalidSigner');
                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
    
    
                // And finally make sure the owner can't do it with someone elses signature
                await expect(etherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigpartsOther.v,
                    sigpartsOther.r,
                    sigpartsOther.s,
                )).to.be.rejectedWith('EtherlessInvalidSigner');
                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
    
            });        
            it("Fails deadline check", async function() {
                const { etherless, domain, types } = await loadFixture(deployEtherlessFixture);
    
                const [owner, otherAccount, thirdParty] = await hre.ethers.getSigners();
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;
                const thirdAddr = thirdParty.address;
    
                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
    
                const thirdEtherless = new hre.ethers.Contract(
                    await etherless.getAddress(),
                    etherless.interface,
                    thirdParty,
                );
    
                const value = 500n;
                const fee = 20n;
    
                const nonce = await thirdEtherless.nonces(ownerAddr);
    
                // Purposefully bad deadline
                const deadline = Math.floor(Date.now() / 1000) - 5000;
    
                const values = {
                    spender: ownerAddr,
                    recipient: otherAddr,
                    value: value,
                    fee: fee,
                    nonce: nonce,
                    deadline: deadline,
                };
    
                const signatureHex = await owner.signTypedData(
                    domain,
                    types,
                    values,
                );
    
                const sigparts = Signature.from(signatureHex);
    
                // NOTE: this fails with 
                // "TypeError: Cannot read properties of undefined (reading 'spender')"
                // Appears to be a bug in the library...
                // const valid = await verifyTypedData({
                //     address: ownerAddr,
                //     domain,
                //     types,
                //     primaryType: 'transferEtherless',
                //     values,
                //     signatureHex,
                //   })
    
                await expect(thirdEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigparts.v,
                    sigparts.r,
                    sigparts.s,
                )).to.be.rejectedWith('EtherlessExpiredSignature');
    
                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
            });
            it("Fails when fee is greater than the balance", async function() {
                const { etherless, domain, types } = await loadFixture(deployEtherlessFixture);
    
                const [owner, otherAccount, thirdParty] = await hre.ethers.getSigners();
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;
                const thirdAddr = thirdParty.address;
    
                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
    
                const thirdEtherless = new hre.ethers.Contract(
                    await etherless.getAddress(),
                    etherless.interface,
                    thirdParty,
                );
    
                const value = 10n;
                const fee = 1000010n;
    
                const nonce = await thirdEtherless.nonces(ownerAddr);
                const deadline = Math.ceil(Date.now() / 1000) + 5000;
                const values = {
                    spender: ownerAddr,
                    recipient: otherAddr,
                    value: value,
                    fee: fee,
                    nonce: nonce,
                    deadline: deadline,
                };
    
                const signatureHex = await owner.signTypedData(
                    domain,
                    types,
                    values,
                );
    
                const sigparts = Signature.from(signatureHex);
                await expect(thirdEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigparts.v,
                    sigparts.r,
                    sigparts.s,
                )).to.be.rejectedWith("ERC20InsufficientBalance");
    
                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
    
            });
            it("Fails when fee+value is greater than the balance", async function() {
                const { etherless, domain, types } = await loadFixture(deployEtherlessFixture);
    
                const [owner, otherAccount, thirdParty] = await hre.ethers.getSigners();
                const otherAddr = otherAccount.address;
                const ownerAddr = owner.address;
                const thirdAddr = thirdParty.address;
    
                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
    
                const thirdEtherless = new hre.ethers.Contract(
                    await etherless.getAddress(),
                    etherless.interface,
                    thirdParty,
                );
    
                const value = 1000000n;
                const fee = 20n;
    
                const nonce = await thirdEtherless.nonces(ownerAddr);
                const deadline = Math.ceil(Date.now() / 1000) + 5000;
                const values = {
                    spender: ownerAddr,
                    recipient: otherAddr,
                    value: value,
                    fee: fee,
                    nonce: nonce,
                    deadline: deadline,
                };
    
                const signatureHex = await owner.signTypedData(
                    domain,
                    types,
                    values,
                );
    
                const sigparts = Signature.from(signatureHex);
                await expect(thirdEtherless.transferEtherless(
                    ownerAddr,
                    otherAddr,
                    value,
                    fee,
                    deadline,
                    sigparts.v,
                    sigparts.r,
                    sigparts.s,
                )).to.be.rejectedWith("ERC20InsufficientBalance");
    
                expect(await etherless.balanceOf(otherAddr)).to.equal(0n);
                expect(await etherless.balanceOf(thirdAddr)).to.equal(0n);
                expect(await etherless.balanceOf(ownerAddr)).to.equal(1000000n);
            });
        });   
    });
}

runEtherlessTests("EtherlessTester");