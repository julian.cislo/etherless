import {runLockableTests} from "./Lockable";
import {runEtherlessTests} from "./Etherless";

describe("Mobby", function () {
    const MobbyContract = "Mobby";
    runLockableTests(MobbyContract);
    runEtherlessTests(MobbyContract);
});