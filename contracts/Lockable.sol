// SPDX-License-Identifier: MIT
pragma solidity ^0.8.23;

import {ERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";
import {Status} from "./Status.sol";

abstract contract Lockable is ERC20, Ownable {
    mapping(address account => uint256) private _lockedBalances;

    error LockableInvalidAddress(address invalid);
    error LockableInsufficientFunds(address lockee, uint256 requested, uint256 available);

    /**
     * @dev Lock event follows:
     * https://github.com/ethereum/EIPs/issues/1132
     * @param lockee the address with funds being locked
     * @param reason always 0 for this contract
     * @param amount total value being locked
     * @param validUntil always 0 for this contract, locks are infinite
     */
    event Lock(
        address indexed lockee,
        uint256 indexed reason,
        uint256 amount,
        uint256 validUntil
    );
    /**
     * @dev Unlock event follows:
     * https://github.com/ethereum/EIPs/issues/1132
     * @param lockee the address with funds being unlocked
     * @param reason always 0 for this contract
     * @param amount total value being unlocked
     */
    event Unlock(
        address indexed lockee,
        uint256 indexed reason,
        uint256 amount
    );

    /**
     * @dev prevent a users funds from transfer, return a status (restriction) code
     * @param lockee the address on which to lock the coins
     * @param amount the amount of which to lock
     *
     * `lockee` cannot be 0.
     *
     * `lockee` must have at least `amount` funds
     * unlocked and available.
     *
     * On success, emits an EIP-1132 compatible `Lock` event
     **/
    function lock(
        address lockee,
        uint256 amount
    ) external onlyOwner {
        if (lockee == address(0)) {
            revert LockableInvalidAddress(lockee);
        }

        uint256 available = availableBalanceOf(lockee);
        if (availableBalanceOf(lockee) < amount) {
            revert LockableInsufficientFunds(lockee, amount, available);
        }

        _lockedBalances[lockee] += amount;
        emit Lock(lockee, 0, amount, 0);
    }

    /**
     * @dev allow user to transfer funds, return a status (restriction) code
     * @param lockee the address on which to unlock the coins
     * @param amount the amount of which to unlock
     *
     * `lockee` cannot be 0.
     *
     * `lockee` must have at least `amount` funds
     * locked.
     *
     * On success, emits an EIP-1132 compatible `Unlock` event
     **/
    function unlock(
        address lockee,
        uint256 amount
    ) external onlyOwner {
        if (lockee == address(0)) {
            revert LockableInvalidAddress(lockee);
        }

        uint256 locked = lockedBalanceOf(lockee);
        if (locked < amount) {
            revert LockableInsufficientFunds(lockee, amount, locked);
        }

        _lockedBalances[lockee] -= amount;
        emit Unlock(lockee, 0, amount);
    }

    /**
     * @dev Creates an `amount` of tokens for the `recipient` address per ERC-5679
     * @param recipient address to send the coins
     * @param amount the total amount to send
     *
     * When the token is being minted, the transfer events MUST be emitted as if
     * the token in the `amount` were transferred from address 0x0 to the recipient
     * address identified by `recipient`.
     *
     * The total supply MUST increase accordingly.
     *
     * You cannot mint `recipient` the address 0x0.
     *
     * Only the contract owner can mint.
     *
     * The tokens must be assigned to the `_lockedBalances` and not affect the
     * _balances.
     */
    function mint(
        address recipient,
        uint256 amount,
        bytes calldata
    ) external onlyOwner {
        // Note: this emits the required Transfer event, ensures no address 0,
        //       and increases total supply
        _mint(recipient, amount);
        _lockedBalances[recipient] += amount;
        emit Lock(recipient, 0, amount, 0);
    }

    /**
     * @dev Destroys an `amount` of tokens of the `burnFrom` address.
     * @param burnFrom burn coins belonging to this address
     * @param amount burn this many coins
     *
     * When the token is being burned, the transfer events MUST be emitted as if
     * the token in the `amount` were transferred from the recipient address
     * identified by `_from` to the address of 0x0.
     *
     * The total supply MUST decrease accordingly.
     *
     * You cannot burn `burnFrom` the address 0x0.
     *
     * Only the token owner (`msg.sender` == `burnFrom`) can burn. We explicitly do
     * not enable any etherless functionality for this operation.
     *
     * You can burn your locked balance, but your unlocked tokens burn first.
     * This will emit an unlock event.
     *
     * The tokens destroyed must be unassigned to the _balances field.
     **/
    function burn(address burnFrom, uint256 amount, bytes calldata) external {
        if (msg.sender != burnFrom || burnFrom == address(0)) {
            revert ERC20InvalidSender(burnFrom);
        }

        super._update(burnFrom, address(0), amount);
        
        // Detect if we burnt locked tokens, and emit appropriate unlock event
        uint256 newBalance = balanceOf(burnFrom);
        if (_lockedBalances[burnFrom] > newBalance) {
            uint256 difference = _lockedBalances[burnFrom] - newBalance;
            _lockedBalances[burnFrom] = newBalance;
            emit Unlock(burnFrom, 0, difference);
        }
    }

    /**
     * @dev return the locked balance for a user
     * @param who the address of the balance we wish to check
     **/
    function lockedBalanceOf(address who) public view returns (uint256) {
        return _lockedBalances[who];
    }

    /**
     * @dev return the available balance for a user
     * @param who the address of the balance we wish to check
     **/
    function availableBalanceOf(address who) public view returns (uint256) {
        return (balanceOf(who) - _lockedBalances[who]);
    }

    /**
     * @dev overrides the update function to check the available balance.
     * @param from the address from which to move funds
     * @param to the addres to which to send funds
     * @param value the amount to transfer
     */
    function _update(
        address from,
        address to,
        uint256 value
    ) internal virtual override {
        uint256 availableBalance = availableBalanceOf(from);
        if (availableBalance < value) {
            if (from != address(0) && to != address(0)) {
                revert ERC20InsufficientBalance(from, availableBalance, value);
            }
        }
        super._update(from, to, value);
    }
}

contract LockableTester is Lockable {
  constructor ()
        ERC20("lockableTestCoin", "xxx")
        Ownable(msg.sender)
        payable
    {}
}