// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0;

/**
 * @title Status Codes Library
 *
 * @dev ERC-1066 codes: 
 *      https://github.com/ethereum/ercs/blob/master/ERCS/erc-1066.md 
 */
library Status {
    enum Code {
        /**
         * 0x0* Generic Codes
         */
        FAILURE,
        SUCCESS,
        AWATING_OTHERS,
        ACCEPTED,
        LOWER_LIMIT,
        RECEIVER_ACTION_REQUESTED,
        UPPER_LIMIT,
        RESERVED_0x07,
        DUPLICATE_UNNECESSARY_OR_INAPPLICABLE,
        RESERVED_0x09,
        RESERVED_0x0A,
        RESERVED_0x0B,
        RESERVED_0x0C,
        RESERVED_0x0D,
        RESERVED_0x0E,
        INFORMATIONAL_OR_METADATA,

        /**
         * 0x1* Permissions & Control
         */
        DISALLOWED_OR_STOP,
        ALLOWED_OR_GO,
        AWAITING_OTHERS_PERMISSION,
        PERMISSION_REQUESTED,
        TOO_OPEN_INSECURE,
        NEEDS_YOUR_PERMISSION_OR_REQUEST_FOR_CONTINUATION,
        REVOKED_OR_BANNED,
        RESERVED_0x17,
        NOT_APPLICABLE_TO_CURRENT_STATE,
        RESERVED_0x19,
        RESERVED_0x1A,
        RESERVED_0x1B,
        RESERVED_0x1C,
        RESERVED_0x1D,
        RESERVED_0x1E,
        PERMISSION_DETAILS_OR_CONTROL_CONDITIONS,

        /**
         * 0x2* Find, Inequalities & Range
         */
        NOT_FOUND_UNEQUAL_OR_OUT_OF_RANGE,
        FOUND_EQUAL_OR_IN_RANGE,
        AWAITING_MATCH,
        MATCH_REQUEST_SENT,
        BELOW_RANGE_OR_UNDERFLOW,
        REQUEST_FOR_MATCH,
        ABOVE_RANGE_OR_OVERFLOW,
        RESERVED_0x27,
        DUPLICATE_CONFLICT_OR_COLLISION,
        RESERVED_0x29,
        RESERVED_0x2A,
        RESERVED_0x2B,
        RESERVED_0x2C,
        RESERVED_0x2D,
        RESERVED_0x2E,
        MATCHING_META_OR_INFO,

        /**
         * 0x3* Negotiation & Governance
         */
        SENDER_DISAGREES_OR_NAY,
        SENDER_AGREES_OR_YEA,
        AWAITING_RATIFICATION,
        OFFER_SENT_OR_VOTED,
        QUORUM_NOT_REACHED,
        RECEIVERS_RATIFICATION_REQUESTED,
        OFFER_OR_VOTE_LIMIT_REACHED,
        RESERVED_0x37,
        ALREADY_VOTED,
        RESERVED_0x39,
        RESERVED_0x3A,
        RESERVED_0x3B,
        RESERVED_0x3C,
        RESERVED_0x3D,
        RESERVED_0x3E,
        NEGOTIATION_RULES_OR_PARTICIPATION_INFO,

        /**
         * 0x4* Availability & Time
         */
        UNAVAILABLE,
        AVAILABLE,
        PAUSED,
        QUEUED,
        NOT_AVAILABLE_YET,
        AWAITING_YOUR_AVAILABILITY,
        EXPIRED,
        RESERVED_0x47,
        ALREADY_DONE,
        RESERVED_0x49,
        RESERVED_0x4A,
        RESERVED_0x4B,
        RESERVED_0x4C,
        RESERVED_0x4D,
        RESERVED_0x4E,
        AVAILABILITY_RULES_OR_INFO,

        /**
         * 0x5* Tokens, Funds & Finance
         */
        TRANSFER_FAILED,
        TRANSFER_SUCCESSFUL,
        AWAITING_PAYMENT_FROM_OTHERS,
        HOLD_OR_ESCROW,
        INSUFFICIENT_FUNDS,
        FUNDS_REQUESTED,
        TRANSFER_VOLUME_EXCEEDED,
        RESERVED_0x57,
        FUNDS_NOT_REQUIRED,
        RESERVED_0x59,
        RESERVED_0x5A,
        RESERVED_0x5B,
        RESERVED_0x5C,
        RESERVED_0x5D,
        RESERVED_0x5E,
        TOKEN_OR_FINANCIAL_INFORMATION,

        /**
         * 0x6* TBD to 0x9* TBD
         */
        RESERVED_0x60,
        RESERVED_0x61,
        RESERVED_0x62,
        RESERVED_0x63,
        RESERVED_0x64,
        RESERVED_0x65,
        RESERVED_0x66,
        RESERVED_0x67,
        RESERVED_0x68,
        RESERVED_0x69,
        RESERVED_0x6A,
        RESERVED_0x6B,
        RESERVED_0x6C,
        RESERVED_0x6D,
        RESERVED_0x6E,
        RESERVED_0x6F,
        RESERVED_0x70,
        RESERVED_0x71,
        RESERVED_0x72,
        RESERVED_0x73,
        RESERVED_0x74,
        RESERVED_0x75,
        RESERVED_0x76,
        RESERVED_0x77,
        RESERVED_0x78,
        RESERVED_0x79,
        RESERVED_0x7A,
        RESERVED_0x7B,
        RESERVED_0x7C,
        RESERVED_0x7D,
        RESERVED_0x7E,
        RESERVED_0x7F,
        RESERVED_0x80,
        RESERVED_0x81,
        RESERVED_0x82,
        RESERVED_0x83,
        RESERVED_0x84,
        RESERVED_0x85,
        RESERVED_0x86,
        RESERVED_0x87,
        RESERVED_0x88,
        RESERVED_0x89,
        RESERVED_0x8A,
        RESERVED_0x8B,
        RESERVED_0x8C,
        RESERVED_0x8D,
        RESERVED_0x8E,
        RESERVED_0x8F,
        RESERVED_0x90,
        RESERVED_0x91,
        RESERVED_0x92,
        RESERVED_0x93,
        RESERVED_0x94,
        RESERVED_0x95,
        RESERVED_0x96,
        RESERVED_0x97,
        RESERVED_0x98,
        RESERVED_0x99,
        RESERVED_0x9A,
        RESERVED_0x9B,
        RESERVED_0x9C,
        RESERVED_0x9D,
        RESERVED_0x9E,
        RESERVED_0x9F,

        /**
         * 0xA* Application-Specific Codes
         */
        APP_SPECIFIC_FAILURE,
        APP_SPECIFIC_SUCCESS,
        APP_SPECIFIC_AWAITING_OTHERS,
        APP_SPECIFIC_ACCEPTANCE,
        APP_SPECIFIC_BELOW_CONDITION,
        APP_SPECIFIC_RECEIVER_ACTION_REQUESTED,
        APP_SPECIFIC_EXPIRY_OR_LIMIT,
        RESERVED_0xA7,
        APP_SPECIFIC_INAPPLICABLE_CONDITION,
        RESERVED_0xA9,
        RESERVED_0xAA,
        RESERVED_0xAB,
        RESERVED_0xAC,
        RESERVED_0xAD,
        RESERVED_0xAE,
        APP_SPECIFIC_META_OR_INFO,

        /**
         * 0xB* TBD to 0xD* TBD
         */
        RESERVED_0xB0,
        RESERVED_0xB1,
        RESERVED_0xB2,
        RESERVED_0xB3,
        RESERVED_0xB4,
        RESERVED_0xB5,
        RESERVED_0xB6,
        RESERVED_0xB7,
        RESERVED_0xB8,
        RESERVED_0xB9,
        RESERVED_0xBA,
        RESERVED_0xBB,
        RESERVED_0xBC,
        RESERVED_0xBD,
        RESERVED_0xBE,
        RESERVED_0xBF,
        RESERVED_0xC0,
        RESERVED_0xC1,
        RESERVED_0xC2,
        RESERVED_0xC3,
        RESERVED_0xC4,
        RESERVED_0xC5,
        RESERVED_0xC6,
        RESERVED_0xC7,
        RESERVED_0xC8,
        RESERVED_0xC9,
        RESERVED_0xCA,
        RESERVED_0xCB,
        RESERVED_0xCC,
        RESERVED_0xCD,
        RESERVED_0xCE,
        RESERVED_0xCF,
        RESERVED_0xD0,
        RESERVED_0xD1,
        RESERVED_0xD2,
        RESERVED_0xD3,
        RESERVED_0xD4,
        RESERVED_0xD5,
        RESERVED_0xD6,
        RESERVED_0xD7,
        RESERVED_0xD8,
        RESERVED_0xD9,
        RESERVED_0xDA,
        RESERVED_0xDB,
        RESERVED_0xDC,
        RESERVED_0xDD,
        RESERVED_0xDE,
        RESERVED_0xDF,

        /**
         * 0xE* Encryption, Identity & Proofs
         */
        DECRYPT_FAILURE,
        DECRYPT_SUCCESS,
        AWAITING_OTHER_SIGNATURES_OR_KEYS,
        SIGNED,
        UNSIGNED_OR_UNTRUSTED,
        SIGNATURE_REQUIRED,
        KNOWN_TO_BE_COMPROMISED,
        RESERVED_0xE7,
        ALREADY_SIGNED_OR_NOT_ENCRYPTED,
        RESERVED_0xE9,
        RESERVED_0xEA,
        RESERVED_0xEB,
        RESERVED_0xEC,
        RESERVED_0xED,
        RESERVED_0xEE,
        CRYPTOGRAPHY_ID_OR_PROOF_METADATA,

        /**
         * 0xF* Off-Chain
         */
        OFF_CHAIN_FAILURE,
        OFF_CHAIN_SUCCESS,
        AWAITING_OFF_CHAIN_PROCESS,
        OFF_CHAIN_PROCESS_STARTED,
        OFF_CHAIN_SERVICE_UNREACHABLE,
        OFF_CHAIN_ACTION_REQUIRED,
        OFF_CHAIN_EXPIRY_OR_LIMIT_REACHED,
        RESERVED_0xF7,
        DUPLICATE_OFF_CHAIN_REQUEST,
        RESERVED_0xF9,
        RESERVED_0xFA,
        RESERVED_0xFB,
        RESERVED_0xFC,
        RESERVED_0xFD,
        RESERVED_0xFE,
        OFF_CHAIN_INFO_OR_META
    }
}

contract StatusCodeTester {
    Status.Code public defaultStatusCode;

    constructor(uint8 defaultCode) payable {
        defaultStatusCode = Status.Code(defaultCode);
    }

    // we return majors for each col
    function getCode(uint8 code) public pure returns (string memory) {
        if (Status.Code(code) == Status.Code.INFORMATIONAL_OR_METADATA) { // 0x0F
            return "INFORMATIONAL_OR_METADATA";
        }
        if (Status.Code(code) == Status.Code.PERMISSION_DETAILS_OR_CONTROL_CONDITIONS) { // 0x1F
            return "PERMISSION_DETAILS_OR_CONTROL_CONDITIONS";
        }
        if (Status.Code(code) == Status.Code.MATCHING_META_OR_INFO) { // 0x2F
            return "MATCHING_META_OR_INFO";
        }
        if (Status.Code(code) == Status.Code.NEGOTIATION_RULES_OR_PARTICIPATION_INFO) { // 0x3F
            return "NEGOTIATION_RULES_OR_PARTICIPATION_INFO";
        }
        if (Status.Code(code) == Status.Code.AVAILABILITY_RULES_OR_INFO) { // 0x4F
            return "AVAILABILITY_RULES_OR_INFO";
        }
        if (Status.Code(code) == Status.Code.TOKEN_OR_FINANCIAL_INFORMATION) { // 0x5F
            return "TOKEN_OR_FINANCIAL_INFORMATION";
        }
        if (Status.Code(code) == Status.Code.RESERVED_0x9F) { // 0x9F:
            return "RESERVED_0x9F";
        }
        if (Status.Code(code) == Status.Code.APP_SPECIFIC_META_OR_INFO) { // 0xAF
            return "APP_SPECIFIC_META_OR_INFO";
        }
        if (Status.Code(code) == Status.Code.RESERVED_0xDF) {
            return "RESERVED_0xDF";
        }
        if (Status.Code(code) == Status.Code.CRYPTOGRAPHY_ID_OR_PROOF_METADATA) { // 0xEF
            return "CRYPTOGRAPHY_ID_OR_PROOF_METADATA"; 
        }
        if (Status.Code(code) == Status.Code.OFF_CHAIN_INFO_OR_META) { // 0xFF
            return "OFF_CHAIN_INFO_OR_META";
        }
        return "UNKNOWN";
    }

    function getDefault() public view returns (Status.Code) {
        return defaultStatusCode;
    }
}