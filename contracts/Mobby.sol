// SPDX-License-Identifier: MIT

pragma solidity ^0.8.23;

import {ERC20Permit} from "@openzeppelin/contracts/token/ERC20/extensions/ERC20Permit.sol";
import {ERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";

import {Lockable} from "./Lockable.sol";
import {Etherless} from "./Etherless.sol";

string constant tokenName = "Mobby";
string constant tokenSymbol = "MOB";

/**
 * @dev Implementation of the Mobby token. This is a Lockable, Etherless token.
 */
contract Mobby is Lockable, Etherless {

    constructor ()
        ERC20Permit(tokenName)
        ERC20(tokenName, tokenSymbol)
        Ownable(msg.sender)
        payable
    {}


    /**
     * @dev overrides the update function to check the available balance.
     * While Lockable is an ERC20, the compiler doesn't feel that way about it
     * so we specify which we are using.
     * @param from the address from which to move funds
     * @param to the addres to which to send funds
     * @param value the amount to transfer
     */
    function _update(
        address from,
        address to,
        uint256 value
    ) internal override(Lockable, ERC20) {
        Lockable._update(from, to, value);
    }
}